# Mallard Music Manager

A program to keep track of one's music collection.

## Features:
- Express server
    - Store and manage album info
- React frontend
    - Add/remove albums (info + cover art)
    - List albums/artists
    - Show one album/artist info
    - Search for album/artist by artist name, album name, song name or year
    - Use MusicBrainz API to search for album info/cover art that can be added to collection