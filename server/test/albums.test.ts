import request from "supertest";
import server from "../src/server.js";

import { Album } from "../src/albumsRouter.js";

const album = {
    name: "Nakki",
    artist: "Vene",
    year: 6969,
    songList: [
        { name: "Saatana", songNumber: 666, length: "66:66" }
    ]
};

let newId = "";

describe("Post album", () => {
    it("returns 400 on missing or invalid parameters", async () => {
        const response = await request(server)
            .post("/albums")
            .send({ nakki: "vene"});
        expect(response.statusCode).toBe(400);
        expect(response.body.error).toBe("400: parameters missing or invalid");
    });
    it("returns 400 on invalid song length", async () => {
        const response = await request(server)
            .post("/albums")
            .send({ ...album, songList: [{ name: "Saatana", songNumber: 1, length: "asdf" }]} );
        expect(response.statusCode).toBe(400);
        expect(response.body.error).toBe("400: song length parameter(s) invalid");
    });
    it("returns 201 with the created album object for a valid album with a generated id", async () => {
        const response = await request(server)
            .post("/albums")
            .send(album);
        newId = response.body.id;
        expect(response.statusCode).toBe(201);
        expect(typeof(newId)).toBe("string");
        expect(response.body.name).toBe(album.name);
        expect(response.body.artist).toBe(album.artist);
        expect(response.body.year).toBe(album.year);
        expect(response.body.songList[0].name).toBe(album.songList[0].name);
        expect(response.body.songList[0].songNumber).toBe(album.songList[0].songNumber);
    });
});

//describe("Post album image", () => {
//    it("returns 404 on wrong id", async () => {
//        const response = await request(server)
//            .post("/albums/img/asdf");
//        expect(response.statusCode).toBe(404);
//        expect(response.body.error).toBe("404: album id not found");
//    });
//    it("returns 400 with wrong content-type header", async () => {
//        const response = await request(server)
//            .post(`/albums/img/${newId}`)
//        expect(response.statusCode).toBe(400);
//        expect(response.body.error).toBe("400: not a valid image type");
//    });
//    it("returns 201 with the created album image for a valid album", async () => {
//        const response = await request(server)
//            .post(`/albums/img/${newId}`)
//            .set("content-type", "image/jpeg")
//            .send(album);
//        newId = response.body.id;
//        expect(response.statusCode).toBe(201);
//        expect(typeof(newId)).toBe("string");
//        expect(response.body.name).toBe(album.name);
//        expect(response.body.artist).toBe(album.artist);
//        expect(response.body.year).toBe(album.year);
//        expect(response.body.songList[0].name).toBe(album.songList[0].name);
//        expect(response.body.songList[0].songNumber).toBe(album.songList[0].songNumber);
//    });
//});

describe("Get all albums", () => {
    it("returns 200 and an object array of all albums", async () => {
        const response = await request(server)
            .get("/albums");
        expect(response.statusCode).toBe(200);
        const fetchedAlbum = response.body.find((e: Album) => e.id === newId);
        expect(fetchedAlbum.name === album.name);
        expect(fetchedAlbum.artist === album.artist);
        expect(fetchedAlbum.year === album.year);
        expect(fetchedAlbum.name === album.name);
        expect(fetchedAlbum.songList[0].name).toBe(album.songList[0].name);
        expect(fetchedAlbum.songList[0].songNumber).toBe(album.songList[0].songNumber);
    });
});

describe("Get one album", () => {
    it("returns 404 on wrong id", async () => {
        const response = await request(server)
            .get("/albums/asdf");
        expect(response.statusCode).toBe(404);
        expect(response.body.error).toBe("404: album id not found");
    });
    it("returns 200 and an album object of valid id", async () => {
        const response = await request(server)
            .get(`/albums/${newId}`);
        expect(response.statusCode).toBe(200);
        expect(response.body.id).toBe(newId);
        expect(response.body.name).toBe(album.name);
        expect(response.body.artist).toBe(album.artist);
        expect(response.body.year).toBe(album.year);
        expect(response.body.songList[0].name).toBe(album.songList[0].name);
        expect(response.body.songList[0].songNumber).toBe(album.songList[0].songNumber);
    });
});

describe("Put", () => {
    it("returns 404 on wrong id", async () => {
        const response = await request(server)
            .put("/albums/asdf")
            .send({ nakki: "vene"});
        expect(response.statusCode).toBe(404);
        expect(response.body.error).toBe("404: album id not found");
    });
    it("returns 400 on parameters of invalid type", async () => {
        const response = await request(server)
            .put(`/albums/${newId}`)
            .send({ name: 1234 });
        expect(response.statusCode).toBe(400);
        expect(response.body.error).toBe("400: one or more of the parameters are of the wrong type");
    });
    it("returns 200 with the updated album object for a valid id and parameters", async () => {
        const response = await request(server)
            .put(`/albums/${newId}`)
            .send({ name: "Wiener" });
        expect(response.statusCode).toBe(200);
        expect(response.body.name).toBe("Wiener");
        expect(response.body.artist).toBe(album.artist);
        expect(response.body.year).toBe(album.year);
        expect(response.body.songList[0].name).toBe(album.songList[0].name);
        expect(response.body.songList[0].songNumber).toBe(album.songList[0].songNumber);
    });
});

//describe("Get album image", () => {
//    it("returns 404 on wrong id", async () => {
//        const response = await request(server)
//            .get("/albums/img/asdf");
//        expect(response.statusCode).toBe(404);
//        expect(response.body.error).toBe("404: album id not found");
//    });
//    it("returns 404 on an id with no imgFile", async () => {
//        const response = await request(server)
//            .get(`/albums/img/${newId}`);
//        expect(response.statusCode).toBe(404);
//        expect(response.body.error).toBe("404: no image for this album");
//    });
//    it("returns 404 on an id with missing image file in filepath", async () => {
//        const response = await request(server)
//            .get(`/albums/img/${newId}`);
//        expect(response.statusCode).toBe(404);
//        expect(response.body.error).toBe("404: no image for this album");
//    });
//    it("returns 200 and album art image of valid id", async () => {
//        const response = await request(server)
//            .get(`/albums/${newId}`);
//        expect(response.statusCode).toBe(200);
//        expect(response.body.id).toBe(newId);
//        expect(response.body.name).toBe(album.name);
//        expect(response.body.artist).toBe(album.artist);
//        expect(response.body.year).toBe(album.year);
//        expect(response.body.songList[0].name).toBe(album.songList[0].name);
//        expect(response.body.songList[0].songNumber).toBe(album.songList[0].songNumber);
//    });
//});


describe("Delete", () => {
    it("returns 404 on wrong id", async () => {
        const response = await request(server)
            .delete("/albums/asdf");
        expect(response.statusCode).toBe(404);
        expect(response.body.error).toBe("404: album id not found");
    });
    it("returns 204 with an empty response for a valid id", async () => {
        const response = await request(server)
            .delete(`/albums/${newId}`);
        expect(response.statusCode).toBe(204);
        expect(response.body.name).toBe(undefined);
    });
});
