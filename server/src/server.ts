import express from "express";
import cors from "cors";
import albumsRouter from "./albumsRouter.js";
import middleware from "./middleware.js";

const server = express();
server.use(cors({ origin: process.env.CLIENT_URL }));
server.use(express.json());

server.use("/albums", albumsRouter);
server.use(middleware.notFound);

export default server;