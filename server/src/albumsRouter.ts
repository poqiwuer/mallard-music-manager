import express, { Request, Response } from "express";
import fs from "fs";
import { v4 as uuidv4 } from "uuid";
import bodyParser from "body-parser";

import middleware from "./middleware.js";

export interface Album {
    [key: string]: string | number | Array<Song> | undefined,
    id: string,
    name: string,
    artist: string,
    year: number,
    songList: Array<Song>,
    imgFile?: string
}

interface Song {
    name: string,
    songNumber: number,
    length?: string // In form hh:mm:ss
}

function writeAlbumsToFile() {
    fs.writeFileSync("./album-db.json", JSON.stringify(albums, null, 2), { encoding: "utf8" });
}

const albums = JSON.parse(fs.readFileSync("./album-db.json", { encoding: "utf8"})) as Array<Album>;

const router = express.Router();

router.delete("/:id", middleware.validateId, (req: Request, res: Response) => {
    const albumIndex = albums.findIndex(album => album.id === req.params.id);
    if (albums[albumIndex].imgFile !== undefined) {
        fs.unlinkSync(`./img/${albums[albumIndex].imgFile}`);
    }
    albums.splice(albumIndex, 1);
    writeAlbumsToFile();
    res.status(204).send();
    console.log("Deleted album " + req.params.id);
});

router.delete("/img/:imgFile", middleware.validateImage, (req: Request, res: Response) => {
    const imgFile =  req.params.imgFile as string;
    const id = imgFile.split(".")[0];
    const album = albums.find(album => album.id === id) as Album;
    album.imgFile = undefined;
    writeAlbumsToFile();
    fs.unlinkSync(`./img/${imgFile}`);
    res.status(204).send();
    console.log("Removed image " + imgFile);
});

router.get("/", (req: Request, res: Response) => {
    res.status(200).send(albums);
    console.log("Sent a list of all albums");
});

router.get("/:id", middleware.validateId, (req: Request, res: Response) => {
    const album = albums.find(album => album.id === req.params.id) as Album;
    res.status(200).send(album);
    console.log("Sent info on album " + album.id);
});

router.get("/img/:imgFile", middleware.validateImage, (req: Request, res: Response) => {
    const imgFile =  req.params.imgFile as string;
    if (imgFile.endsWith(".jpg")) {
        res.setHeader("content-type", "image/jpeg");
    } else {
        res.setHeader("content-type", "image/png");
    }
    res.status(200).sendFile(`${process.cwd()}/img/${imgFile}`);
    console.log("Sent image " + req.params.imgFile);
});

router.post("/", middleware.handlePost, (req: Request, res: Response) => {
    const newAlbum = { id: uuidv4(), name: req.body.name, artist: req.body.artist, year: req.body.year, songList: req.body.songList } as Album;
    albums.push(newAlbum);
    writeAlbumsToFile();
    res.status(201).send(newAlbum);
    console.log("Created new album " + newAlbum.id);
});

router.post("/img/:id", middleware.validateId, bodyParser.raw({ type: ["image/jpeg", "image/png"], limit: "5mb" }), (req: Request, res: Response) => {
    const img = req.body;
    if (Object.keys(img).length === 0) {
        res.status(400).send({ error: "400: not a valid image type" });
        return;
    }
    const album = albums.find(album => album.id === req.params.id) as Album;
    let imgFile = album.id;
    let imgType = "";
    if (req.headers["content-type"] === "image/jpeg") {
        imgFile += ".jpg";
        imgType = "image/jpeg";
    } else if (req.headers["content-type"] === "image/png") {
        imgFile += ".png";
        imgType = "image/png";
    }
    try {
        fs.writeFileSync(`./img/${imgFile}`, img);
    } catch (error) {
        res.send(error);
        return;
    }
    album.imgFile = imgFile;
    writeAlbumsToFile();
    res.setHeader("content-type", imgType);
    res.status(200).send(imgFile);
    console.log("Created image art for album " + album.id);
});

router.put("/:id", middleware.validateId, middleware.handlePut, (req: Request, res: Response) => {
    const album = albums.find(album => album.id === req.params.id) as Album;
    for (const key of Object.keys(album)) {
        if (req.body[key] !== undefined && key !== "id") {
            album[key] = req.body[key];
        }
    }
    writeAlbumsToFile();
    res.status(200).send(album);
    console.log("Changed album " + album.id);
});

export default router;
export { albums };