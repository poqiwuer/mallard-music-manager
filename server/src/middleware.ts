import fs from "fs";
import { Request, Response, NextFunction } from "express";
import { albums, Album } from "./albumsRouter.js";

function handlePost(req: Request, res: Response, next: NextFunction): void {
    const body = req.body;
    if (typeof(body.name) !== "string" || 
        typeof(body.artist) !== "string" ||
        typeof(body.year) !== "number" ||
        typeof(body.songList) !== "object"
    ) {
        res.status(400).send({ error: "400: parameters missing or invalid" });
        return;
    }
    for (const song of body.songList) {
        if (typeof(song.name) !== "string" || typeof(song.songNumber) !== "number") {
            res.status(400).send({ error: "400: parameters missing or invalid" });
            return;
        }
        if (song.length !== undefined) {
            const lengthArray = song.length.split(":");
            if (lengthArray.length > 3) {
                res.status(400).send({ error: "400: song length parameter(s) invalid" });
                return;
            }
            for (const timeFragment of lengthArray) {
                if (isNaN(Number(timeFragment))) {
                    res.status(400).send({ error: "400: song length parameter(s) invalid" });
                    return;
                }
            }
        }
    }
    next();
}

function handlePut(req: Request, res: Response, next: NextFunction) {
    const album = albums.find(album => album.id == req.params.id) as Album;
    for (const key of Object.keys(album)) {
        if (typeof(req.body[key]) !== typeof(album[key]) && req.body[key] !== undefined) {
            res.status(400).send({ error: "400: one or more of the parameters are of the wrong type" });
            return;
        }
    }
    next();
}

function notFound(req: Request, res: Response): void {
    res.status(404).send({ error: "404: endpoint not found" });
}

function validateId(req: Request, res: Response, next: NextFunction): void {
    const album = albums.find(album => album.id === req.params.id);
    if (album === undefined) {
        res.status(404).send({ error: "404: album id not found" });
        return;
    }
    next();
}

function validateImage(req: Request, res: Response, next: NextFunction): void {
    if (!fs.existsSync(`./img/${req.params.imgFile}`)) {
        res.status(404).send({ error: "404: image not found"});
        return;
    }
    next();
}

export default { handlePost, handlePut, notFound, validateId, validateImage };