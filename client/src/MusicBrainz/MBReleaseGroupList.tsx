import React from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import Link from "@mui/material/Link";

import { MBReleaseGroup } from "./AddFromMusicBrainz";

function MBReleaseGroupList(props: { 
  results: Array<MBReleaseGroup>, 
  searchRelease: (releaseGroupId: string) => void
  }) {

  return (
    <List>
      {(props.results as Array<MBReleaseGroup>).map(releaseGroup =>
        <ListItem key={releaseGroup.id}>
          <Link component="button" onClick={() => props.searchRelease(releaseGroup.id)}>
            {releaseGroup["artist-credit"].length === 1 ?
              <>{releaseGroup["artist-credit"][0].name} - </>
              :
              <>{releaseGroup["artist-credit"][0].name} et al. - </>
            }
            {releaseGroup.title}
            {releaseGroup.releases.length > 0 &&
            <>, {releaseGroup.releases.length} release(s)</>
            }
          </Link>
        </ListItem>
      )}
    </List>
  );
}

export default MBReleaseGroupList;