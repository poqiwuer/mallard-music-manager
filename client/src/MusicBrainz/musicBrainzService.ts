import axios from "axios";

const baseUrl = "http://musicbrainz.org/ws/2";
const coverArtUrl = "http://coverartarchive.org/release";
const searchOptions = "fmt=json&limit=10"; // get results in json form, limit search results to 10
const lookUpOptions = "fmt=json";

function getCoverArtURL(releaseId: string) {
  return `${coverArtUrl}/${releaseId}/front-500`;
}

function getReleases(releaseGroupId: string) {
  return axios.get(`${baseUrl}/release?release-group=${releaseGroupId}&inc=artist-credits+release-groups&${lookUpOptions}`).then(res => res.data["releases"]);
}

function getReleaseGroups(artistId: string) {
  return axios.get(`${baseUrl}/release-group?query=arid:${artistId}&inc=artist-credits&${lookUpOptions}`).then(res => res.data["release-groups"]);
}

function getReleaseInfo(releaseId: string) {
  return axios.get(`${baseUrl}/release/${releaseId}?inc=recordings+artists&${lookUpOptions}`).then(res => res.data);
}

function searchArtist(query: string) {
  return axios.get(`${baseUrl}/artist?query=name:${query}&${searchOptions}`).then(res => res.data["artists"]);
}

function searchReleaseGroup(query: string) {
  return axios.get(`${baseUrl}/release-group?query=name:${query}&${searchOptions}`).then(res => res.data["release-groups"]);
}

export default { getCoverArtURL, getReleases, getReleaseGroups, getReleaseInfo, searchArtist, searchReleaseGroup };