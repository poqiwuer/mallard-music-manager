import React from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import Link from "@mui/material/Link";

import musicBrainzService from "./musicBrainzService";
import { MBRelease, MBSong, ResultType } from "./AddFromMusicBrainz";

function MBReleaseList(props: { 
  results: Array<MBRelease>, 
  setResults: (newResults: MBRelease) => void, // newResults not being an array is intentional
  setResultType: (newResultType: ResultType) => void,
  setArtistName: (newArtistName: string) => void,
  setSongList: (newSongList: Array<MBSong>) => void,
  setCoverArtURL: (newCoverArtURL: string | undefined) => void
  }) {
  function listReleaseInfo(id: string) {
    musicBrainzService.getReleaseInfo(id)
      .then(res => {
        props.setResults(res as MBRelease);
        props.setResultType("releaseInfo");
        if (res["artist-credit"] !== undefined) {
          if (res["artist-credit"].length === 1) {
            props.setArtistName(res["artist-credit"][0].name);
          } else {
            props.setArtistName(res["artist-credit"][0].name + " et al.");
          }
        } else {
          props.setArtistName("");
        }
        if (res.media !== undefined) {
          // This way of doing this ensures that the numbering is correct even if there are multiple CDs or other release media
          const songList = [] as Array<MBSong>;
          let i = 0;
          for (const medium of res.media) {
            for (const song of medium.tracks) {
              songList.push({ id: song.id, title: song.title, number: String(++i), length: song.length} );
            }
          }
          props.setSongList(songList);
        } else {
          props.setSongList([]);
        }
        if (res["cover-art-archive"].front) {
          props.setCoverArtURL(musicBrainzService.getCoverArtURL(res.id));
        } else {
          props.setCoverArtURL(undefined);
        }
      });
  }

  return (
    <List>
      {props.results.map(release => 
        <ListItem key={release.id}>
          <Link component="button" onClick={() => listReleaseInfo(release.id)}>
            {release.country}, {release.date} - {release.title}
            {release["cover-art-archive"].front &&
              <> (Cover art available)</>
            }
          </Link>
        </ListItem>
      )
      }
    </List>
  );
}

export default MBReleaseList;