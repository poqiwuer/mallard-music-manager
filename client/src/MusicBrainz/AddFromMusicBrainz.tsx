import React, { useState } from "react";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import Button from "@mui/material/Button";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import Link from "@mui/material/Link";

import AlbumForm from "../AlbumForm";
import { Album, AlbumNoId } from "../albumService";
import musicBrainzService from "./musicBrainzService";
import MBArtistList from "./MBArtistList";
import MBReleaseGroupList from "./MBReleaseGroupList";
import MBReleaseList from "./MBReleaseList";
import MBReleaseInfoList from "./MBReleaseInfoList";

export interface MBArtist {
  id: string,
  name: string,
  country: string,
  disambiguation: string
}

export interface MBReleaseGroup {
  id: string,
  title: string,
  releases: Array<{ id: string, title: string }>,
  "artist-credit": Array<{ name: string }>
}

export interface MBRelease {
  id: string,
  title: string,
  "artist-credit"?: Array<{ name: string }>
  date: string,
  country: string,
  "cover-art-archive": { front: boolean },
  media?: Array<{ tracks: Array<MBSong> }>
}

export interface MBSong {
  id: string,
  title: string,
  number: string,
  length: number // in ms, apparently
}

export type ResultType = "artist" | "releaseGroup" | "release" | "releaseInfo";
export type SearchingOrAddingType = "searching" | "adding";

function AddFromMusicBrainz(props: { allAlbums: Array<Album>, setAllAlbums: (newAllAlbums: Array<Album>) => void }) {
  const [searchingOrAdding, setSearchingOrAdding] = useState("searching" as SearchingOrAddingType);
  const [searchBy, setSearchBy] = useState("Artist");
  const [query, setQuery] = useState("");
  const [results, setResults] = useState([] as Array<MBArtist> | Array<MBReleaseGroup> | Array<MBRelease> | MBRelease);
  const [resultType, setResultType] = useState("artist" as ResultType);
  const [artistName, setArtistName] = useState("");
  const [artistId, setArtistId] = useState("");
  const [albumName, setAlbumName] = useState("");
  const [albumId, setAlbumId] = useState("");
  const [songList, setSongList] = useState([] as Array<MBSong>);
  const [coverArtURL, setCoverArtURL] = useState(undefined as string | undefined);
  const [baseAlbumData, setBaseAlbumData] = useState(undefined as undefined | AlbumNoId);

  function searchArtist(name: string) {
    musicBrainzService.searchArtist(name)
      .then(res => {
        setResults(res as Array<MBArtist>);
        setResultType("artist");
        setArtistName("");
        setArtistId("");
        setAlbumName("");
        setAlbumId("");
      });
  }

  function searchReleaseGroup(name: string) {
    musicBrainzService.searchReleaseGroup(name)
      .then(res => {
        setResults(res as Array<MBReleaseGroup>);
        setResultType("releaseGroup");
        setArtistName("");
        setArtistId("");
        setAlbumName("");
        setAlbumId("");
      });
  }

  function searchReleaseGroupByArtist(id: string) {
    musicBrainzService.getReleaseGroups(id)
      .then(res => {
        setResults(res);
        setResultType("releaseGroup");
        setArtistName(res[0]["artist-credit"][0].name);
        setArtistId(res[0]["artist-credit"][0].artist.id);
      });
  }

  function searchRelease(id: string) {
    musicBrainzService.getReleases(id)
      .then(res => {
        setResults(res);
        setResultType("release");
        console.log(id);
        console.log(res);
        setArtistName(res[0]["artist-credit"][0].name);
        setArtistId(res[0]["artist-credit"][0].artist.id);
        setAlbumName(res[0]["release-group"].title);
        setAlbumId(res[0]["release-group"].id);
      });
  }

  function submit(e: React.FormEvent) {
    e.preventDefault();
    if (searchBy === "Artist") {
      searchArtist(query);
    } else {
      searchReleaseGroup(query);
    }
  }

  function ResultList() {
    if (resultType === "artist") {
      return <MBArtistList
        results={results as Array<MBArtist>}
        searchReleaseGroupsByArtist={searchReleaseGroupByArtist}
      />;
    } else if (resultType === "releaseGroup") {
      return <MBReleaseGroupList
        results={results as Array<MBReleaseGroup>}
        searchRelease={searchRelease}
      />;
    } else if (resultType === "release") {
      return <MBReleaseList
        results={results as Array<MBRelease>}
        setResults={setResults}
        setResultType={setResultType}
        setArtistName={setArtistName}
        setSongList={setSongList}
        setCoverArtURL={setCoverArtURL}
      />;
    } else {
      return <MBReleaseInfoList 
        release={results as MBRelease}
        songList={songList}
        artistName={artistName}
        coverArtURL={coverArtURL}
        setSearchingOrAdding={setSearchingOrAdding}
        setBaseAlbumData={setBaseAlbumData}
      />;
    }
  }

  return (
    <Grid container spacing={4} marginBottom={2}>
      <Grid item xs={12}>
        <Typography variant="h4">Add from MusicBrainz:</Typography>
      </Grid>
      {searchingOrAdding === "searching" ?
        <React.Fragment>
          <Grid item xs={12}>
            <form onSubmit={submit}>
              <Grid container spacing={4}>
                <Grid item xs={12} sm={8}>
                  <TextField
                    fullWidth
                    onChange={e => setQuery(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12} sm={4}>
                  <FormControl fullWidth>
                    <InputLabel>Search by</InputLabel>
                    <Select
                      label="Search by"
                      value={searchBy}
                      onChange={e => setSearchBy(e.target.value)}
                    >
                      <MenuItem value="Artist">Artist</MenuItem>
                      <MenuItem value="Album">Album</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item xs={12}>
                  <Button variant="contained" type="submit">Search</Button>
                </Grid>
              </Grid>
            </form>
          </Grid>
          <Grid item xs={12}>
            <Breadcrumbs>
              {artistName !== "" &&
                <Link component="button" underline="hover" onClick={() => searchReleaseGroupByArtist(artistId)}>{artistName}</Link>
              }
              {albumName !== "" &&
                <Link component="button" underline="hover" onClick={() => searchRelease(albumId)}>{albumName}</Link>
              }
            </Breadcrumbs>
          </Grid>
          <Grid item xs={12}>
            <ResultList/>
          </Grid>
        </React.Fragment>
        :
        <React.Fragment>
          <AlbumForm allAlbums={props.allAlbums} setAllAlbums={props.setAllAlbums} baseAlbumData={baseAlbumData} baseCoverArtURL={coverArtURL}/>
        </React.Fragment>
      }
    </Grid>
  );
}

export default AddFromMusicBrainz;