import React from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import Link from "@mui/material/Link";

import { MBArtist } from "./AddFromMusicBrainz";

function MBArtistList(props: { 
  results: Array<MBArtist>, 
  searchReleaseGroupsByArtist: (artistId: string) => void
  }) {

  return (
    <List>
      {props.results.map(artist =>
        <ListItem key={artist.id}>
          <Link component="button" onClick={() => {
            props.searchReleaseGroupsByArtist(artist.id);
          }}>
            {artist.name}
            {artist.country !== undefined &&
            <> ({artist.country})</>
            }
            {artist.disambiguation !== undefined &&
            <>, {artist.disambiguation}</>
            }
          </Link>
        </ListItem>
      )}
    </List>
  );
}

export default MBArtistList;