import React from "react";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import Button from "@mui/material/Button";

import { MBRelease, MBSong, SearchingOrAddingType } from "./AddFromMusicBrainz";
import { AlbumNoId } from "../albumService"; 

function milliseconds2lengthString(milliseconds: number) {
  const minutes = Math.floor(milliseconds / 60000);
  const seconds = Math.round((milliseconds % 60000) / 1000);
  if (seconds >= 10) {
    return String(minutes) + ":" + String(seconds);
  } else {
    return String(minutes) + ":0" + String(seconds);
  }
}

function MBReleaseInfoList(props: { 
  release: MBRelease, 
  songList: Array<MBSong>, 
  artistName: string, 
  coverArtURL: string | undefined, 
  setSearchingOrAdding: (newSearchingOrAdding: SearchingOrAddingType) => void,
  setBaseAlbumData: (newBaseAlbumData: AlbumNoId) => void
  }) {

  function setAddTemplate() {
    const newSongList = props.songList.map(song => { 
      return {
        name: song.title,
        songNumber: Number(song.number),
        length: milliseconds2lengthString(song.length)
      };
    });
    const albumData: AlbumNoId = {
      name: props.release.title,
      artist: props.artistName,
      year: Number(props.release.date.split("-")[0]), // gets only the year from dates of type 2010-09-01
      songList: newSongList
    };
    props.setBaseAlbumData(albumData);
    props.setSearchingOrAdding("adding");
  }

  return (
    <Grid container spacing={4}>
      <Grid item xs={12} sm={6}>
        <Typography variant="h6">{props.artistName} - {props.release.title} ({props.release.date})</Typography>
        <List>
          {props.songList.map(song => 
            <ListItem key={song.id}>{song.number} - {song.title}</ListItem>
          )}
        </List>
      </Grid>
      <Grid item xs={12} sm={6}>
        {props.coverArtURL !== "" &&
          <img src={props.coverArtURL} height="300px"/>
        }
      </Grid>
      <Grid item xs={12}>
        <Button onClick={setAddTemplate}>Add new album from this template</Button>
      </Grid>
    </Grid>
  );
}

export default MBReleaseInfoList;