import React from "react";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";

import { Album } from "./albumService";
import ListAlbums from "./ListAlbums";

function ListAllAlbums(params: { allAlbums: Array<Album> }) {
  return (
    <Grid item xs={12} marginBottom={4}>
      <Typography variant="h4" marginBottom={5}>List of all albums:</Typography>
      <ListAlbums albums={params.allAlbums}/>
    </Grid>
  );
}

export default ListAllAlbums;