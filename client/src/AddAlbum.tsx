import React from "react";
import Typography from "@mui/material/Typography";

import AlbumForm from "./AlbumForm";
import { Album } from "./albumService";

function AddAlbum(props: { allAlbums: Array<Album>, setAllAlbums: (newAllAlbums: Array<Album>) => void }) {

  return (
    <React.Fragment>
      <Typography variant="h4" marginBottom={5}>Add new album info:</Typography>
      <AlbumForm allAlbums={props.allAlbums} setAllAlbums={props.setAllAlbums}/>
    </React.Fragment>
  );
}

export default AddAlbum;