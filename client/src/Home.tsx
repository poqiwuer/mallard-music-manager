import React from "react";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

function Home() {
  return (
    <Box>
      <Typography variant="h2" component="h1" gutterBottom>Mallard Music manager</Typography>
      <Typography variant="h4" component="h2" gutterBottom>Music manager for those who have too much time on their hands</Typography>
    </Box>
  );
}

export default Home;