import React, { useEffect, useState } from "react";
import Link from "@mui/material/Link";
import { Link as RouterLink } from "react-router-dom";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import { Album } from "./albumService";

function ListAlbums(params: { albums: Array<Album> }) {

  const [sortedAlbums, setSortedAlbums] = useState([] as Array<Album>);
  useEffect(() => {
    setSortedAlbums([...params.albums].sort((a, b) => {
      // Alphabetic sort by artist, then by year for the same artist
      if (a.artist < b.artist) {
        return -1;
      } else if (a.artist > b.artist) {
        return 1;
      } else {
        return a.year - b.year;
      }
    }));
  }, [params.albums]);

  return (
    <List>
      {sortedAlbums.map(album => 
        <ListItem key={album.id}>
          <Link component={RouterLink} to={"/album/" + album.id}>{album.artist} - {album.name} ({album.year})</Link>
        </ListItem>
      )}
    </List>
  );
}

export default ListAlbums;