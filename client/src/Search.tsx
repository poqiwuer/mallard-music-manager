import React, { useState } from "react";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";

import ListAlbums from "./ListAlbums";
import { Album } from "./albumService";

type SearchBy = "Artist" | "Album" | "Year" | "Song name";

function Search(props: { allAlbums: Array<Album> }) {
  const [searchBy, setSearchBy] = useState("Artist" as SearchBy);
  const [foundAlbums, setFoundAlbums] = useState([] as Array<Album>);

  function search(text: string) {
    text = text.toLowerCase();
    if (searchBy === "Artist") {
      const newFoundAlbums = props.allAlbums.filter(album => 
        album.artist.toLowerCase().includes(text)
      );
      setFoundAlbums(newFoundAlbums);
    } else if (searchBy === "Album") {
      const newFoundAlbums = props.allAlbums.filter(album => 
        album.name.toLowerCase().includes(text)
      );
      setFoundAlbums(newFoundAlbums);
    } else if (searchBy === "Year") {
      const newFoundAlbums = props.allAlbums.filter(album => 
        String(album.year).includes(text)
      );
      setFoundAlbums(newFoundAlbums);
    } else if (searchBy === "Song name") {
      const newFoundAlbums = props.allAlbums.filter(album => {
        for (const song of album.songList) {
          if (song.name.toLowerCase().includes(text)) {
            return true;
          }
        }
        return false;
      });
      setFoundAlbums(newFoundAlbums);
    }
  }

  return (
    <Grid container spacing={4}>
      <Grid item xs={12}>
        <Typography variant="h4">Search albums:</Typography>
      </Grid>
      <Grid item xs={12} sm={8}>
        <TextField
          fullWidth
          onChange={e => search(e.target.value)}
        />
      </Grid>
      <Grid item xs={12} sm={4}>
        <FormControl fullWidth>
          <InputLabel>Search by</InputLabel>
          <Select
            label="Search by"
            value={searchBy}
            onChange={e => setSearchBy(e.target.value as SearchBy)}
          >
            <MenuItem value="Artist">Artist</MenuItem>
            <MenuItem value="Album">Album</MenuItem>
            <MenuItem value="Year">Year</MenuItem>
            <MenuItem value="Song name">Song name</MenuItem>
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <ListAlbums albums={foundAlbums}/>
      </Grid>
    </Grid>
  );
}

export default Search;