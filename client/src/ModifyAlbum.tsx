import React from "react";
import { useParams } from "react-router-dom";
import Typography from "@mui/material/Typography";

import AlbumForm from "./AlbumForm";
import { Album } from "./albumService";

function ModifyAlbum(props: { allAlbums: Array<Album>, setAllAlbums: (newAllAlbums: Array<Album>) => void }) {
  const { id } = useParams();
  const albumExists = props.allAlbums.find(album => album.id === id) === undefined ? false : true;

  return (
    <React.Fragment>
      {albumExists ?
        <React.Fragment>
          <Typography variant="h4" marginBottom={5}>Modify album info:</Typography>
          <AlbumForm allAlbums={props.allAlbums} setAllAlbums={props.setAllAlbums} albumId={id}/>
        </React.Fragment>
        :
        <Typography variant="h6">404: album not found</Typography>
      }
    </React.Fragment>
  );
}

export default ModifyAlbum;