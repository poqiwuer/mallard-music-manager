import React, { useState, useEffect } from "react";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";

import albumService, { Album, AlbumNoId, Song } from "./albumService";

function AlbumForm(props: { allAlbums: Array<Album>, setAllAlbums: (newAllAlbums: Array<Album>) => void, albumId?: string, baseAlbumData?: AlbumNoId, baseCoverArtURL?: string }) {
  const [albumData, setAlbumData] = useState(setInitialAlbumData());
  const [image, setImage] = useState(undefined as File | undefined);
  const [imageURL, setImageURL] = useState("");
  const [submitMessages, setSubmitMessages] = useState([] as Array<string>);

  function validSongLengths() {
    for (const song of albumData.songList) {
      if (song.length === undefined || song.length === "") {
        continue;
      }
      const textArray = song.length.split(":");
      if (textArray.length > 3) {
        return false;
      }
      for (const element of textArray) {
        if (isNaN(Number(element))) {
          return false;
        }
      }
    }
    return true;
  }

  function setInitialAlbumData() {
    if (props.albumId === undefined) {
      if (props.baseAlbumData === undefined) {
        return { name: "", artist: "", year: undefined, songList: [{ name: "", songNumber: 1, length: undefined } as Song]};
      } else {
        return props.baseAlbumData;
      }
    }
    const album = props.allAlbums.find(album => album.id === props.albumId) as Album;
    return { name: album.name, artist: album.artist, year: album.year, songList: album.songList };
  }

  function submitAlbum(event: React.FormEvent) {
    event.preventDefault();
    if (!validSongLengths()) {
      setSubmitMessages(["One or more song lengths invalid"]);
      return;
    }
    setSubmitMessages([]);
    if (props.albumId === undefined) {
      albumService.addAlbum(albumData.name, albumData.artist, albumData.year as number, albumData.songList)
        .then(res => {
          props.allAlbums.push(res);
          props.setAllAlbums([...props.allAlbums]);
          const messages = ["Album added successfully!"];
          if (image !== undefined) {
            albumService.addAlbumArt(res.id, image)
              .then(imgRes => {
                res.imgFile = imgRes;
                messages.push("Album art added successfully!");
                setSubmitMessages(messages);
              });
          } else if (imageURL !== "") {
            fetch(imageURL)
              .then(res => res.blob())
              .then(blob => {
                const imageFile = new File([blob], imageURL, { type: "image/jpeg" });
                albumService.addAlbumArt(res.id, imageFile)
                  .then(imgRes => {
                    res.imgFile = imgRes;
                    messages.push("Album art added successfully!");
                    setSubmitMessages(messages);
                  });
              });
          } else {
            setSubmitMessages(messages);
          }
        });
    } else {
      albumService.modifyAlbum(props.albumId, albumData)
        .then(res => {
          const currentAlbumIndex = props.allAlbums.findIndex(album => album.id === props.albumId);
          props.allAlbums[currentAlbumIndex] = res;
          props.setAllAlbums([...props.allAlbums]);
          const messages = ["Album modified successfully!"];
          if (image !== undefined) {
            albumService.addAlbumArt(res.id, image)
              .then(imgRes => {
                res.imgFile = imgRes;
                messages.push("Album art modified successfully!");
                setSubmitMessages(messages);
              });
          } else if (imageURL === "" && res.imgFile !== undefined) {
            albumService.removeAlbumArt(res.imgFile)
              .then(() => {
                res.imgFile = undefined;
                messages.push("Album art deleted successfully!");
                setSubmitMessages(messages);
              });
          } else {
            setSubmitMessages(messages);
          }
        });
    }
  }

  useEffect(() => {
    if (props.albumId !== undefined) {
      const album = props.allAlbums.find(album => album.id === props.albumId) as Album;
      if (album.imgFile !== undefined) {
        setImageURL(`${albumService.baseUrl}/img/${album.imgFile}`);
      }
    } else if (props.baseCoverArtURL !== undefined) {
      setImageURL(props.baseCoverArtURL);
    }
  }, []);

  return (
    <form onSubmit={submitAlbum}>
      <Grid container spacing={4} alignItems="center">
        <Grid item xs={12}>
          {submitMessages.length > 0 &&
            <List>
              {submitMessages.map(message => 
                <ListItem key={message}>
                  <Typography variant="h6">{message}</Typography>
                </ListItem>
              )}
            </List>
          }
        </Grid>
        <Grid item xs={12} sm={5}>
          <TextField
            required
            id="albumName"
            name="albumName"
            label="Album name"
            value={albumData.name}
            fullWidth
            variant="standard"
            onChange={e => setAlbumData({ ...albumData, name: e.target.value })}
          />
        </Grid>
        <Grid item xs={12} sm={5}>
          <TextField
            required
            id="artistName"
            name="artistName"
            label="Artist name"
            value={albumData.artist}
            fullWidth
            variant="standard"
            onChange={e => setAlbumData({ ...albumData, artist: e.target.value })}
          />
        </Grid>
        <Grid item xs={12} sm={2}>
          <TextField
            required
            id="year"
            name="year"
            label="Year"
            value={albumData.year}
            fullWidth
            variant="standard"
            type="number"
            onChange={e => setAlbumData({ ...albumData, year: Number(e.target.value) })}
          />
        </Grid>
        <Grid item xs={12} marginTop={6}>
          <Typography variant="h5">Song list:</Typography>
        </Grid>
        <Grid item xs={1}>
          <Typography>#</Typography>
        </Grid>
        <Grid item xs={7}>
          <Typography>Name *</Typography>
        </Grid>
        <Grid item xs={4}>
          <Typography>Length (hh:mm:ss)</Typography>
        </Grid>
        {albumData.songList.map(song => 
          <React.Fragment key={song.songNumber}>
            <Grid item xs={1}>
              <Typography>{song.songNumber}</Typography>
            </Grid>
            <Grid item xs={7}>
              <TextField
                required
                fullWidth
                variant="standard"
                value={song.name}
                onChange={e => {
                  song.name = e.target.value;
                  setAlbumData({ ...albumData, songList: [...albumData.songList] });
                }}
              />
            </Grid>
            <Grid item xs={2}>
              <TextField
                fullWidth
                variant="standard"
                value={song.length}
                onChange={e => {
                  song.length = e.target.value;
                  if (song.length === "") {
                    song.length = undefined;
                  }
                  setAlbumData({ ...albumData, songList: [...albumData.songList] });
                }}
              />
            </Grid>
            <Grid item xs={2}>
              {(song.songNumber == albumData.songList.length && song.songNumber >= 2) &&
              <Button 
                variant="outlined" 
                color="warning"
                onClick={() => {
                  albumData.songList.pop();
                  setAlbumData({ ...albumData, songList: [...albumData.songList] });
                }}
              >
                Remove song
              </Button>
              }
            </Grid>
          </React.Fragment>
        )}
        <Grid item xs={12} marginBottom={6}>
          <Button 
            variant="outlined" 
            onClick={() => 
              setAlbumData({...albumData, songList: [...albumData.songList, { name: "", songNumber: albumData.songList.length + 1 }]})}
          >
            Add new song
          </Button>
        </Grid>
        <Grid item xs={3}>
          <Button component="label" variant="contained" color="secondary">
            Upload album art
            <input 
              type="file" 
              hidden 
              accept="image/jpeg, image/png" 
              onChange={e => {
                if (e.target.files !== null) {
                  setImage(e.target.files[0]);
                  setImageURL(URL.createObjectURL(e.target.files[0]));
                  e.target.value = ""; // makes sure React notices the change if the image is removed and then uploaded again
                }
              }}
            />
          </Button>
        </Grid>
        {imageURL !== "" ?
          <React.Fragment>
            <Grid item xs={7} textAlign="right">
              <img src={imageURL} height="150px"></img>
            </Grid>
            <Grid item xs={2}>
              <Button variant="contained" color="warning" onClick={() => {
                setImage(undefined);
                setImageURL("");
              }
              }>
                Remove
              </Button>
            </Grid>
          </React.Fragment>
          :
          <Grid item xs={9}>
          </Grid>
        }
        <Grid item xs={12} marginBottom={10}>
          <Button type="submit" variant="contained">Submit</Button>
        </Grid>
      </Grid>
    </form>
  );
}

export default AlbumForm;