import axios from "axios";
const baseUrl = process.env.REACT_APP_SERVER_URL + "/albums";

export interface Album {
  id: string,
  name: string,
  artist: string,
  year: number,
  songList: Array<Song>,
  imgFile?: string
}

export interface AlbumNoId {
  name: string,
  artist: string,
  year: number,
  songList: Array<Song>,
  imgFile?: string
}

export interface Song {
  name: string,
  songNumber: number
  length?: string
}

function addAlbum(name: string, artist: string, year: number, songList: Array<Song>) {
  const data = {
    name: name,
    artist: artist,
    year: year,
    songList: songList
  };
  return axios.post(baseUrl, data).then(res => res.data as Album);
}

function addAlbumArt(id: string, file: File) {
  return axios.post(`${baseUrl}/img/${id}`, file, { headers: {"Content-Type": file.type} }).then(res => res.data);
}

function getAll() {
  return axios.get(baseUrl).then(res => res.data as Array<Album>);
}

function getOne(id: string) {
  return axios.get(`${baseUrl}/${id}`).then(res => res.data as Album);
}

function modifyAlbum(id: string, data: object) {
  return axios.put(`${baseUrl}/${id}`, data).then(res => res.data as Album);
}

function removeAlbum(id: string) {
  return axios.delete(`${baseUrl}/${id}`).then(res => res.data);
}

function removeAlbumArt(imgFile: string) {
  return axios.delete(`${baseUrl}/img/${imgFile}`).then(res => res.data);
}

export default { addAlbum, addAlbumArt, baseUrl, getAll, getOne, modifyAlbum, removeAlbum, removeAlbumArt };