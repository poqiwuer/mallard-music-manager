import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Routes, Route, Link as RouterLink } from "react-router-dom";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import CssBaseline from "@mui/material/CssBaseline";
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";

import albumService, { Album } from "./albumService";

import Home from "./Home";
import Search from "./Search";
import ListAllAlbums from "./ListAllAlbums";
import AddAlbum from "./AddAlbum";
import AlbumInfo from "./AlbumInfo";
import ModifyAlbum from "./ModifyAlbum";
import AddFromMusicBrainz from "./MusicBrainz/AddFromMusicBrainz";

function App() {

  const [allAlbums, setAllAlbums] = useState([] as Array<Album>);

  useEffect(() => {
    albumService.getAll()
      .then(albums => setAllAlbums(albums));
  }, []);

  return (
    <React.Fragment>
      <CssBaseline/>
      <Container>
        <Router>
          <Grid container padding={4}>
            <Grid item xs={4} sm={2}><Link component={RouterLink} to="/">Home</Link></Grid>
            <Grid item xs={4} sm={2}><Link component={RouterLink} to="/search">Search</Link></Grid>
            <Grid item xs={4} sm={2}><Link component={RouterLink} to="/list">List</Link></Grid>
            <Grid item xs={4} sm={2}><Link component={RouterLink} to="/add">Add</Link></Grid>
            <Grid item xs={8} sm={4}><Link component={RouterLink} to="/add-mb">Add (MusicBrainz)</Link></Grid>
          </Grid>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/search" element={<Search allAlbums={allAlbums}/>}/>
            <Route path="/list" element={<ListAllAlbums allAlbums={allAlbums}/>}/>
            <Route path="/add" element={<AddAlbum allAlbums={allAlbums} setAllAlbums={setAllAlbums}/>}/>
            <Route path="/album/:id" element={<AlbumInfo allAlbums={allAlbums} setAllAlbums={setAllAlbums}/>}/>
            <Route path="/modify/:id" element={<ModifyAlbum allAlbums={allAlbums} setAllAlbums={setAllAlbums}/>}/>
            <Route path="/add-mb" element={<AddFromMusicBrainz allAlbums={allAlbums} setAllAlbums={setAllAlbums}/>}/>
          </Routes>
        </Router>
      </Container>
    </React.Fragment>
  );
}

export default App;
