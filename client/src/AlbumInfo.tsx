import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Box from "@mui/material/Box";
import { Link as RouterLink } from "react-router-dom";
import Button from "@mui/material/Button";

import albumService, { Album } from "./albumService";

function AlbumInfo(params: { allAlbums: Array<Album>, setAllAlbums: (newAllAlbums: Array<Album>) => void }) {
  const { id } = useParams();
  let album = params.allAlbums.find(album => album.id === id);
  const [albumMissingMessage, setAlbumMissingMessage] = useState("404: album not found");
  const [albumLength, setAlbumLength] = useState(undefined as string | undefined);

  function deleteAlbum() {
    albumService.removeAlbum(id as string)
      .then(() => {
        const newAllAlbums = params.allAlbums.filter(deletedAlbum => deletedAlbum.id === id ? false : true);
        params.setAllAlbums(newAllAlbums);
        album = undefined;
        setAlbumMissingMessage("Album deleted successfully!");
      });
  }

  function lengthString2seconds(length: string): number {
    const lengthArray = length.split(":");
    let seconds = 0;
    for (const index in lengthArray) {
      seconds += Number(lengthArray[index]) * 60 ** (lengthArray.length - Number(index) - 1);
    }
    return seconds;
  }

  function seconds2lengthString(totalSeconds: number): string {
    const hours = Math.floor(totalSeconds / 3600);
    const minutes = Math.floor((totalSeconds % 3600) / 60);
    const seconds = totalSeconds % 60;
    let lengthString = "";
    if (hours >= 10) {
      lengthString += String(hours) + ":";
    } else if (hours >= 1) {
      lengthString += "0" + String(hours) + ":";
    } // if hours = 0, do not include it at all
    lengthString += minutes >= 10 ? String(minutes) + ":" : "0" + String(minutes) + ":";
    lengthString += seconds >= 10 ? String(seconds) : "0" + String(seconds);
    return lengthString;
  }

  useEffect(() => {
    if (album === undefined) {
      return;
    }
    let seconds = 0;
    for (const song of album.songList) {
      if (song.length === undefined) {
        return;
      } else {
        seconds += lengthString2seconds(song.length);
      }
    }
    setAlbumLength(seconds2lengthString(seconds));

  });

  return (
    <Grid container columnSpacing={4} rowSpacing={4} alignItems="center">
      {album === undefined ? (
        <Grid item xs={12}>
          <Typography variant="h6">{albumMissingMessage}</Typography>
        </Grid>
      ) : (
        <React.Fragment>
          <Grid item md={6} lg={7}>
            <Typography variant="h4" gutterBottom>{album.artist}</Typography>
            <Typography variant="h3" gutterBottom>{album.name}</Typography>
            <Box width="90%">
              <Table>
                <TableBody>
                  {album.songList.map(song => 
                    <TableRow key={song.songNumber}>
                      <TableCell>{song.songNumber}</TableCell>
                      <TableCell>{song.name}</TableCell>
                      <TableCell align="right">{song.length}</TableCell>
                    </TableRow>
                  )}
                  {albumLength !== undefined &&
                    <TableRow>
                      <TableCell sx={{ borderBottom: 0 }}></TableCell>
                      <TableCell sx={{ borderBottom: 0 }}></TableCell>
                      <TableCell sx={{ borderBottom: 0 }} align="right">{albumLength}</TableCell>
                    </TableRow>
                  }
                </TableBody>
              </Table>
            </Box>
          </Grid>
          {album.imgFile !== undefined && 
            <Grid item md={6} lg={5} ><img height="400px" src={process.env.REACT_APP_SERVER_URL + "/albums/img/" + album.imgFile}/></Grid>
          }
          <Grid item xs={6} marginTop={10} marginBottom={4}>
            <Button component={RouterLink} to={"/modify/" + id}>Modify</Button>
          </Grid>
          <Grid item xs={6} textAlign="right" marginTop={10} marginBottom={4}>
            <Button color="warning" onClick={deleteAlbum}>Delete</Button>
          </Grid>
        </React.Fragment>
      )}
    </Grid>
  );
}

export default AlbumInfo;